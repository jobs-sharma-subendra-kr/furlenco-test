Step 1:
To build spring boot docker image

From the furlenco-test/javaapp directory use:
> sudo mvn install dockerfile:build

Step 2:
From furlenco-test directory
First Configuring
> sudo docker-compose config

To start all docker containers 
> sudo docker-compose up

To stop all docker containers
> sudo docker-compose down

## Please bear in mind I renamed 1 query param i.e. classes to grades ##
so to filter results you'll have to use
e.g
/students?grades=1,2

## Use page=value and size=value for pagination
e.g
/students?grades=1,2&page=1&size=100

The database will be pre populated with 1001 dummy entries with "John Doe" values

## How to use on postman
by current configuration, you can access the server via localhost port 3000
you can change the port from docker-compose file

e.g.
localhost:3000/students/?grades=1,2&page=1&size=10

## Incase the database is not updating 
you might want to remove volumes

Docker version : Docker version 18.09.0, build 4d60db4
Docker compose version : docker-compose version 1.23.2, build 1110ad01
maven version : Apache Maven 3.5.2
jdk version : 8